<?php

/**
 * @file
 * Hooks for the field_extra module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the private value.
 *
 * @param bool $private
 *   Current private value.
 * @param array $context
 *   Context of the query (entity_type, field_name).
 */
function hook_field_extra_private_alter(&$private, array $context) {
  if ('node' == $context['entity_type']) {
    $private = TRUE;
  }
}

/**
 * @} End of "addtogroup hooks".
 */
