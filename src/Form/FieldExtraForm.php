<?php

namespace Drupal\field_extra\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for adding private settings.
 */
class FieldExtraForm extends ConfigFormBase {

  /**
   * Returns the entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_extra_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'field_extra.private_fields',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('field_extra.private_fields');

    $options = [];

    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if (in_array(FieldableEntityInterface::class, class_implements($definition->getOriginalClass())) &&
        in_array(EntityOwnerInterface::class, class_implements($definition->getOriginalClass()))
      ) {
        $label = $definition->getLabel() instanceof TranslatableMarkup ? $definition->getLabel()->render() : $definition->getLabel();
        $options[$definition->id()] = $label;
      }
    }

    $form['types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity types'),
      '#options' => $options,
      '#default_value' => !empty($config->get('types')) ? $config->get('types') : [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('field_extra.private_fields')
      ->set('types', array_filter($form_state->getValue('types')))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
