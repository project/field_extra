<?php

namespace Drupal\field_extra;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for field extra module.
 */
class FieldExtraPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Returns the entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new FieldExtraPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Returns an array of field extra permissions.
   *
   * @return array
   *   An array of permissions for all plugins.
   */
  public function permissions() {
    $permissions = [];

    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if (in_array(FieldableEntityInterface::class, class_implements($definition->getOriginalClass())) &&
        in_array(EntityOwnerInterface::class, class_implements($definition->getOriginalClass()))
      ) {

        $label = $definition->getLabel() instanceof TranslatableMarkup ? $definition->getLabel()->render() : $definition->getLabel();
        $permissions["field extra access {$definition->id()} private fields"] = [
          'title' => $this->t('Access @entity_type private fields', ['@entity_type' => $label]),
        ];
      }
    }

    return $permissions;
  }

}
