<?php

namespace Drupal\field_extra;

use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Manager for field extra related methods.
 */
class FieldExtraManager implements FieldExtraManagerInterface {

  /**
   * Returns the database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Returns the module_handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a FieldExtraManager object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Base Database API class.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Interface for classes that manage a set of enabled modules.
   */
  public function __construct(Connection $database, ModuleHandlerInterface $module_handler) {
    $this->database = $database;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function isPrivate(int $id, string $type): bool {
    [$entity_type, $field_name] = explode('__', $type);

    $query = $this->database->select('field_extra_value', 'pf');
    $query->fields('pf', ['private']);
    $query->condition('pf.id', $id);
    $query->condition('pf.entity_type', $entity_type);
    $query->condition('pf.field_name', $field_name);

    $private = (bool) $query->execute()->fetchField();
    $context = [
      'entity_type' => $entity_type,
      'field_name' => $field_name,
    ];
    $this->moduleHandler->alter('field_extra_private', $private, $context);

    return $private;
  }

  /**
   * {@inheritdoc}
   */
  public function add(array $conditions) {
    $query = $this->database->merge('field_extra_value');
    $query->keys($conditions);
    $query->fields(['private' => 1]);
    $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $conditions) {
    $query = $this->database->delete('field_extra_value');
    foreach ($conditions as $key => $value) {
      $query->condition($key, $value);
    }
    $query->execute();
  }

}
