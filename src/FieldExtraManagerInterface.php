<?php

namespace Drupal\field_extra;

/**
 * Interface for field extra service.
 */
interface FieldExtraManagerInterface {

  /**
   * Returns whether user has private enabled.
   *
   * @param int $id
   *   The entity id.
   * @param string $type
   *   The key of the value.
   *
   * @return bool
   *   True if private enabled for the user's field.
   */
  public function isPrivate(int $id, string $type): bool;

  /**
   * Add data to the private table.
   *
   * @param array $conditions
   *   An array of conditions; e.g. entity id, entity_type, etc.
   */
  public function add(array $conditions);

  /**
   * Delete data from the private table.
   *
   * @param array $conditions
   *   An array of conditions; e.g. entity id, entity_type, etc.
   */
  public function delete(array $conditions);

}
