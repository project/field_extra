<?php

namespace Drupal\field_extra\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns a table with all private fields.
 */
class FieldExtraController extends ControllerBase {

  /**
   * Show all private fields.
   */
  public function fields() {
    $header = [
      ['data' => $this->t('Field name')],
      ['data' => $this->t('Entity type')],
      ['data' => $this->t('Entity bundle')],
      ['data' => $this->t('Operations')],
    ];

    $rows = [];
    $field_configs = $this->entityTypeManager()->getStorage('field_config')->loadMultiple();
    $field_ui = $this->moduleHandler()->moduleExists('field_ui');
    $config = $this->config('field_extra.private_fields');

    /** @var \Drupal\field\Entity\FieldConfig $field_config */
    foreach ($field_configs as $field_config) {
      $entity_type = $field_config->getTargetEntityTypeId();

      if (!empty($config->get('types')[$entity_type]) &&
        $settings = $field_config->getThirdPartySettings('field_extra')
      ) {
        if (!empty($settings['private_field'])) {
          $rows[] = [
            $field_config->label(),
            $entity_type,
            $field_config->getTargetBundle(),
            $field_ui ? $field_config->toLink($this->t('Edit'), "{$entity_type}-field-edit-form") : '',
          ];
        }
      }
    }

    return [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('There are no private fields.'),
    ];
  }

}
