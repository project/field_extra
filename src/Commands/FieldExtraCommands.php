<?php

namespace Drupal\field_extra\Commands;

use Drupal\Core\Database\Connection;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;

/**
 * Class FieldExtraCommands. The base class for Drush commands.
 */
class FieldExtraCommands extends DrushCommands {

  /**
   * Returns the database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a FieldExtraCommands object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Base Database API class.
   */
  public function __construct(Connection $database) {
    parent::__construct();
    $this->database = $database;
  }

  /**
   * Remove all private fields from the database.
   *
   * @throws \Exception
   *
   * @usage drush field-extra:delete
   *   Delete all private fields from the database.
   *
   * @command field-extra:delete
   * @aliases fed
   */
  public function deletePrivateFields() {
    if (!$this->io()->confirm(dt('Are you sure you want to delete all private fields?'))) {
      throw new UserAbortException();
    }

    $this->database->delete('field_extra_value')->execute();
    $this->output()->writeln(dt('All private fields have been removed.'));
  }

}
