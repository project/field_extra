# Field extra

Provides private entity fields functionality.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/field_extra).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/field_extra).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Configure permissions:
Home >> Administration >> People
(/admin/people/permissions/module/field_extra)

- Configure Field extra:
Home >> Administration >> Configuration >> Content authoring
(/admin/config/content/private-settings)


## Maintainers

- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
