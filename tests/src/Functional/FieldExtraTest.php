<?php

namespace Drupal\Tests\field_extra\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\Tests\node\Functional\NodeTestBase;

/**
 * Class FieldExtraTest. The base class for testing private fields.
 */
class FieldExtraTest extends NodeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'field_extra'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test private fields.
   */
  public function testPrivateFields() {
    // Log in as an admin user with permission to manage private settings.
    $admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin);

    // Enable private fields for node content type.
    $config = \Drupal::configFactory()->getEditable('field_extra.private_fields');
    $config->set('types', ['node' => 'node']);
    $config->save();

    // Create a new node type.
    $this->createContentType(['type' => 'private']);

    // Allow to mark the body field as private.
    $field = FieldConfig::loadByName('node', 'private', 'body');
    $field->setThirdPartySetting('field_extra', 'private_field', TRUE);

    // Check the status of the page.
    $this->drupalGet('node/add/private');
    $this->assertSession()->statusCodeEquals(200);

    // Create a new node with a random name and body.
    $edit = [];
    $edit['title[0][value]'] = $this->randomMachineName(8);
    $edit['body[0][value]'] = $this->randomMachineName(16);
    $this->drupalGet('node/add/private');
    $this->submitForm($edit, 'Save');

    // Get created node.
    $node = $this->drupalGetNodeByTitle($edit['title[0][value]']);

    // Mark the body field as private.
    $conditions = [
      'entity_type' => 'node',
      'field_name' => 'body',
      'id' => $node->id(),
    ];
    \Drupal::service('field_extra.manager')->add($conditions);

    // Check if the body field exists on the page.
    $this->drupalGet("node/{$node->id()}");
    $this->assertSession()->pageTextContains($edit['title[0][value]']);
    $this->assertSession()->pageTextContains($edit['body[0][value]']);

    // Log in as a regular user without private access.
    $user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($user);

    // Check if the body field doesn't exist on the page.
    $this->drupalGet("node/{$node->id()}");
    $this->assertSession()->pageTextContains($edit['title[0][value]']);
    $this->assertSession()->pageTextNotContains($edit['body[0][value]']);

    // Log in as a regular user with private access.
    $user = $this->drupalCreateUser([
      'access content',
      'field extra access node private fields',
    ]);
    $this->drupalLogin($user);

    // Check if the body field exists on the page.
    $this->drupalGet("node/{$node->id()}");
    $this->assertSession()->pageTextContains($edit['title[0][value]']);
    $this->assertSession()->pageTextContains($edit['body[0][value]']);
  }

}
